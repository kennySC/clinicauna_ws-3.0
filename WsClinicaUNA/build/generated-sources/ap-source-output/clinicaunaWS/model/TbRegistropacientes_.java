package clinicaunaWS.model;

import clinicaunaWS.model.TbDetallecita;
import clinicaunaWS.model.TbEncabezadoexpediente;
import clinicaunaWS.model.TbPersona;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-11-05T16:53:41")
@StaticMetamodel(TbRegistropacientes.class)
public class TbRegistropacientes_ { 

    public static volatile SingularAttribute<TbRegistropacientes, TbPersona> idpers;
    public static volatile SingularAttribute<TbRegistropacientes, Integer> versionpaciente;
    public static volatile SingularAttribute<TbRegistropacientes, String> generopaciente;
    public static volatile ListAttribute<TbRegistropacientes, TbDetallecita> tbDetallecitaList;
    public static volatile SingularAttribute<TbRegistropacientes, Date> fechanacimpaciente;
    public static volatile ListAttribute<TbRegistropacientes, TbEncabezadoexpediente> tbEncabezadoexpedienteList;
    public static volatile SingularAttribute<TbRegistropacientes, Long> idpaciente;
    public static volatile SingularAttribute<TbRegistropacientes, String> numtelefpaciente;

}