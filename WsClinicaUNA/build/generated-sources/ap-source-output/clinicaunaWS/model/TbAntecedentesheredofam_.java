package clinicaunaWS.model;

import clinicaunaWS.model.TbEncabezadoexpediente;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-11-05T16:53:41")
@StaticMetamodel(TbAntecedentesheredofam.class)
public class TbAntecedentesheredofam_ { 

    public static volatile SingularAttribute<TbAntecedentesheredofam, String> enfermheredanteced;
    public static volatile SingularAttribute<TbAntecedentesheredofam, String> parentescoanteced;
    public static volatile SingularAttribute<TbAntecedentesheredofam, Integer> versionantecedheredfam;
    public static volatile SingularAttribute<TbAntecedentesheredofam, TbEncabezadoexpediente> idencabexp;
    public static volatile SingularAttribute<TbAntecedentesheredofam, Long> idantecedheredfam;

}