// DTO DEL WEB SERVICE //

package clinicaunaWS.model;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Matthew Miranda
 */

@XmlRootElement(name = "MantusuariosDto")
@XmlAccessorType(XmlAccessType.FIELD)
public class MantusuariosDto {
    
    private Long idus;
    private String tipous;
    private String usernameus;
    private String passwordus;
    private String estadous;
    private String idiomaus;
    private PersonaDto idpers;
//    private List<MantmedicosDto> tbMantmedicosList;
//    private List<DetallecitaDto> tbDetallecitaList;
    
    private boolean nuevo;
    
    // CONSTRUCTOR VACIO //
    public MantusuariosDto(){
        
    }

    public MantusuariosDto(Long idus, String nombreus, String apellidosus, String correous, String tipous, String usernameus, String passwordus, String estadous, String cedulaus, String idiomaus, PersonaDto idpers) {
        this.idus = idus;
        this.tipous = tipous;
        this.usernameus = usernameus;
        this.passwordus = passwordus;
        this.estadous = estadous;
        this.idiomaus = idiomaus;
        this.idpers = idpers;
    }
    
    public MantusuariosDto(TbMantusuarios usuario){
        this.idus = usuario.getIdus();
        this.tipous = usuario.getTipous();
        this.usernameus = usuario.getUsernameus();
        this.passwordus = usuario.getPasswordus();
        this.estadous = usuario.getEstadous();        
        this.idiomaus = usuario.getIdiomaus();
        this.idpers = new PersonaDto(usuario.getIdpers());
    }

//    public void convertirListaMedicos(List<TbMantmedicos> listMed){
//        this.tbMantmedicosList = new ArrayList<>();
//        if(listMed!=null && !listMed.isEmpty()){
//            for(TbMantmedicos med : listMed){
//                MantmedicosDto meddto = new MantmedicosDto(med);
//                this.tbMantmedicosList.add(meddto);
//            }
//        }
//    }
//    
//    public void convertirListaDetalles(List<TbDetallecita> listDetalles){
//        this.tbDetallecitaList = new ArrayList<>();
//        if(listDetalles!=null && !listDetalles.isEmpty()){
//            for(TbDetallecita detcit : listDetalles){
//                DetallecitaDto detcitDto = new DetallecitaDto(detcit);
//                this.tbDetallecitaList.add(detcitDto);
//            }
//        }
//    }
    
    public Long getIdUs() {
        return idus;
    }

    public void setIdUs(Long idUs) {
        this.idus = idUs;
    }   

    public String getTipous() {
        return tipous;
    }

    public void setTipous(String tipous) {
        this.tipous = tipous;
    }

    public String getUsernameus() {
        return usernameus;
    }

    public void setUsernameus(String usernameus) {
        this.usernameus = usernameus;
    }

    public String getPasswordus() {
        return passwordus;
    }

    public void setPasswordus(String passwordus) {
        this.passwordus = passwordus;
    }

    public String getEstadous() {
        return estadous;
    }

    public void setEstadous(String estadous) {
        this.estadous = estadous;
    }

    public String getIdiomaus() {
        return idiomaus;
    }

    public void setIdiomaus(String idiomaus) {
        this.idiomaus = idiomaus;
    }


    public Long getIdus() {
        return idus;
    }

    public void setIdus(Long idus) {
        this.idus = idus;
    }

    public PersonaDto getIdpers() {
        return idpers;
    }

    public void setIdpers(PersonaDto idpers) {
        this.idpers = idpers;
    }

//    public List<MantmedicosDto> getTbMantmedicosList() {
//        return tbMantmedicosList;
//    }
//
//    public void setTbMantmedicosList(List<MantmedicosDto> tbMantmedicosList) {
//        this.tbMantmedicosList = tbMantmedicosList;
//    }
//
//    public List<DetallecitaDto> getTbDetallecitaList() {
//        return tbDetallecitaList;
//    }
//
//    public void setTbDetallecitaList(List<DetallecitaDto> tbDetallecitaList) {
//        this.tbDetallecitaList = tbDetallecitaList;
//    }

    public boolean isNuevo() {
        return nuevo;
    }

    public void setNuevo(boolean nuevo) {
        this.nuevo = nuevo;
    }
    
    @Override
    public String toString() {
        return "MantusuariosDto{" + "idus=" + idus + ", tipous=" + tipous + ", usernameus=" + usernameus + ", passwordus=" + passwordus + ", estadous=" + estadous + ", idiomaus=" + idiomaus + ", idpers=" + idpers + '}';
    }

    

    
    
    
}
