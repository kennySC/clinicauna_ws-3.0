package clinicaunaWS.model;

import java.io.Serializable;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.QueryHint;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Kenneth Sibaja
 */
@Entity
@Table(name = "TB_MANTMEDICOS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbMantmedicos.findAll", query = "SELECT t FROM TbMantmedicos t", hints = @QueryHint(name = "eclipselink.refresh", value = "true"))
    , @NamedQuery(name = "TbMantmedicos.findByIdmed", query = "SELECT t FROM TbMantmedicos t WHERE t.idmed = :idmed", hints = @QueryHint(name = "eclipselink.refresh", value = "true"))
    , @NamedQuery(name = "TbMantmedicos.findByCodigomed", query = "SELECT t FROM TbMantmedicos t WHERE t.codigomed = :codigomed", hints = @QueryHint(name = "eclipselink.refresh", value = "true"))
    , @NamedQuery(name = "TbMantmedicos.findByFoliomed", query = "SELECT t FROM TbMantmedicos t WHERE t.foliomed = :foliomed", hints = @QueryHint(name = "eclipselink.refresh", value = "true"))
    , @NamedQuery(name = "TbMantmedicos.findByCarnemed", query = "SELECT t FROM TbMantmedicos t WHERE t.carnemed = :carnemed", hints = @QueryHint(name = "eclipselink.refresh", value = "true"))
    , @NamedQuery(name = "TbMantmedicos.findByInijornadamed", query = "SELECT t FROM TbMantmedicos t WHERE t.inijornadamed = :inijornadamed", hints = @QueryHint(name = "eclipselink.refresh", value = "true"))
    , @NamedQuery(name = "TbMantmedicos.findByFinjornadamed", query = "SELECT t FROM TbMantmedicos t WHERE t.finjornadamed = :finjornadamed", hints = @QueryHint(name = "eclipselink.refresh", value = "true"))
    , @NamedQuery(name = "TbMantmedicos.findByEspaciosmed", query = "SELECT t FROM TbMantmedicos t WHERE t.espaciosmed = :espaciosmed", hints = @QueryHint(name = "eclipselink.refresh", value = "true"))
    , @NamedQuery(name = "TbMantmedicos.findByEstadomed", query = "SELECT t FROM TbMantmedicos t WHERE t.estadomed = :estadomed", hints = @QueryHint(name = "eclipselink.refresh", value = "true"))
    , @NamedQuery(name = "TbMantmedicos.findByIdPers_nom_ape_ced_carne", query = "SELECT t FROM TbMantmedicos t WHERE UPPER(t.idpers.nombrepers) like UPPER(:nombre) and UPPER(t.idpers.apellidospersona) like UPPER (:apellidos) and UPPER(t.idpers.cedulapers) like UPPER(:cedula) ", hints = @QueryHint(name = "eclipselink.refresh", value = "true"))/*and t.carnemed like :carnemed*/
})
public class TbMantmedicos implements Serializable {

    @Basic(optional = false)
    @Column(name = "CODIGOMED")
    private Long codigomed;    
    @Basic(optional = false)    
    @Column(name = "FOLIOMED")   
    private Long foliomed;
    @Basic(optional = false)
    @Column(name = "CARNEMED")
    private Integer carnemed;
    @Basic(optional = false)
    @Column(name = "ESPACIOSMED")
    private Integer espaciosmed;
    @Basic(optional = false)
    @Version
    @Column(name = "VERSIONMED")
    private Integer versionmed;
                

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @SequenceGenerator(name = "TB_MEDICOS_IDMED_GENERATOR", sequenceName = "CLINICAUNA.SEQ_MANTMEDICOS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TB_MEDICOS_IDMED_GENERATOR")
    @Basic(optional = false)
    @Column(name = "IDMED")
    private Long idmed;
    @Basic(optional = false)
    @Column(name = "INIJORNADAMED")
    @Temporal(TemporalType.TIMESTAMP)
    private Date inijornadamed;
    @Basic(optional = false)
    @Column(name = "FINJORNADAMED")
    @Temporal(TemporalType.TIMESTAMP)
    private Date finjornadamed;
    @Column(name = "ESTADOMED")
    private String estadomed;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idmed", fetch = FetchType.LAZY)
    private List<TbDetallecita> tbDetallecitaList;
    @JoinColumn(name = "IDUS", referencedColumnName = "IDUS")
    @ManyToOne(fetch = FetchType.LAZY)
    private TbMantusuarios idus;
    @JoinColumn(name = "IDPERS", referencedColumnName = "IDPERS")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private TbPersona idpers;

    public TbMantmedicos() {
    }

    public TbMantmedicos(Long idmed) {
        this.idmed = idmed;
    }

    public TbMantmedicos(MantmedicosDto medico) {
        if (medico.getIdmed() != null) {
            this.idmed = medico.getIdmed();
        }
        this.codigomed = medico.getCodigomed();
        this.foliomed = medico.getFoliomed();
        this.carnemed = medico.getCarnemed();
        this.inijornadamed = Date
                .from(medico.getInijornadamed().atZone(ZoneId.systemDefault())
                        .toInstant());
        this.finjornadamed = Date
                .from(medico.getFinjornadamed().atZone(ZoneId.systemDefault())
                        .toInstant());
        this.espaciosmed = medico.getEspaciosmed();
        this.idus = new TbMantusuarios(medico.getIdus());
        this.idpers = new TbPersona(medico.getIdPers());
        
    }

    public TbMantmedicos(Long codigomed, Long foliomed, Integer carnemed, Integer espaciosmed, Integer versionmed, Long idmed, Date inijornadamed, Date finjornadamed, String estadomed, List<TbDetallecita> tbDetallecitaList, TbMantusuarios idus, TbPersona idpers) {
        this.codigomed = codigomed;
        this.foliomed = foliomed;
        this.carnemed = carnemed;
        this.espaciosmed = espaciosmed;
        this.versionmed = versionmed;
        this.idmed = idmed;
        this.inijornadamed = inijornadamed;
        this.finjornadamed = finjornadamed;
        this.estadomed = estadomed;
        this.tbDetallecitaList = tbDetallecitaList;
        this.idus = idus;
        this.idpers = idpers;
    }

    public void actualizarMedico(MantmedicosDto medico) {
        this.idmed = medico.getIdmed();
        this.codigomed = medico.getCodigomed();
        this.foliomed = medico.getFoliomed();
        this.carnemed = medico.getCarnemed();
        this.inijornadamed = Date
                .from(medico.getInijornadamed().atZone(ZoneId.systemDefault())
                        .toInstant());
        this.finjornadamed = Date
                .from(medico.getFinjornadamed().atZone(ZoneId.systemDefault())
                        .toInstant());
        this.espaciosmed = medico.getEspaciosmed();
        this.idus = new TbMantusuarios(medico.getIdus());
        this.idpers = new TbPersona(medico.getIdPers());
    }

    public Long getIdmed() {
        return idmed;
    }

    public void setIdmed(Long idmed) {
        this.idmed = idmed;
    }

    public Date getInijornadamed() {
        return inijornadamed;
    }

    public void setInijornadamed(Date inijornadamed) {
        this.inijornadamed = inijornadamed;
    }

    public Date getFinjornadamed() {
        return finjornadamed;
    }

    public void setFinjornadamed(Date finjornadamed) {
        this.finjornadamed = finjornadamed;
    }

    public String getEstadomed() {
        return estadomed;
    }

    public void setEstadomed(String estadomed) {
        this.estadomed = estadomed;
    }

    @XmlTransient
    public List<TbDetallecita> getTbDetallecitaList() {
        return tbDetallecitaList;
    }

    public void setTbDetallecitaList(List<TbDetallecita> tbDetallecitaList) {
        this.tbDetallecitaList = tbDetallecitaList;
    }

    public TbMantusuarios getIdus() {
        return idus;
    }

    public void setIdus(TbMantusuarios idus) {
        this.idus = idus;
    }

    public TbPersona getIdpers() {
        return idpers;
    }

    public void setIdpers(TbPersona idpers) {
        this.idpers = idpers;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idmed != null ? idmed.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbMantmedicos)) {
            return false;
        }
        TbMantmedicos other = (TbMantmedicos) object;
        if ((this.idmed == null && other.idmed != null) || (this.idmed != null && !this.idmed.equals(other.idmed))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "clinicaunaWS.model.TbMantmedicos[ idmed=" + idmed + " ]";
    }

    public Long getCodigomed() {
        return codigomed;
    }

    public void setCodigomed(Long codigomed) {
        this.codigomed = codigomed;
    }

    public Long getFoliomed() {
        return foliomed;
    }

    public void setFoliomed(Long foliomed) {
        this.foliomed = foliomed;
    }

    public Integer getCarnemed() {
        return carnemed;
    }

    public void setCarnemed(Integer carnemed) {
        this.carnemed = carnemed;
    }

    public Integer getEspaciosmed() {
        return espaciosmed;
    }

    public void setEspaciosmed(Integer espaciosmed) {
        this.espaciosmed = espaciosmed;
    }

    public Integer getVersionmed() {
        return versionmed;
    }

    public void setVersionmed(Integer versionmed) {
        this.versionmed = versionmed;
    }

}
