
package clinicaunaWS.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.QueryHint;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Kenneth Sibaja
 */
@Entity
@Table(name = "TB_ENCABEZADOEXPEDIENTE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbEncabezadoexpediente.findAll", query = "SELECT t FROM TbEncabezadoexpediente t", hints = @QueryHint(name = "eclipselink.refresh", value = "true"))
    , @NamedQuery(name = "TbEncabezadoexpediente.findByIdencabexp", query = "SELECT t FROM TbEncabezadoexpediente t WHERE t.idencabexp = :idencabexp", hints = @QueryHint(name = "eclipselink.refresh", value = "true"))
    , @NamedQuery(name = "TbEncabezadoexpediente.findByHospitalizacionesencabexp", query = "SELECT t FROM TbEncabezadoexpediente t WHERE t.hospitalizacionesencabexp = :hospitalizacionesencabexp", hints = @QueryHint(name = "eclipselink.refresh", value = "true"))
    , @NamedQuery(name = "TbEncabezadoexpediente.findByOperacionesencabexp", query = "SELECT t FROM TbEncabezadoexpediente t WHERE t.operacionesencabexp = :operacionesencabexp", hints = @QueryHint(name = "eclipselink.refresh", value = "true"))    
    , @NamedQuery(name = "TbEncabezadoexpediente.findByAlergiasencabexp", query = "SELECT t FROM TbEncabezadoexpediente t WHERE t.alergiasencabexp = :alergiasencabexp", hints = @QueryHint(name = "eclipselink.refresh", value = "true"))
    , @NamedQuery(name = "TbEncabezadoexpediente.findByIdPaciente", query = "SELECT t FROM TbEncabezadoexpediente t WHERE t.idpaciente.idpaciente = :idPaciente", hints = @QueryHint(name = "eclipselink.refresh", value = "true"))
    , @NamedQuery(name = "TbEncabezadoexpediente.findByPatologicoencabexp", query = "SELECT t FROM TbEncabezadoexpediente t WHERE t.patologicoencabexp = :patologicoencabexp", hints = @QueryHint(name = "eclipselink.refresh", value = "true"))})
public class TbEncabezadoexpediente implements Serializable {

    @Basic(optional = false)
    @Version
    @Column(name = "VERSIONENCABEXP")
    private Integer versionencabexp;
    @Column(name = "HOSPITALIZACIONESENCABEXP")
    private Integer hospitalizacionesencabexp;
    @Column(name = "OPERACIONESENCABEXP")
    private Integer operacionesencabexp;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idencabexp", fetch = FetchType.LAZY)
    private List<TbAntecedentesheredofam> tbAntecedentesheredofamList;

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @SequenceGenerator(name = "TB_ENCABEZADOEXPEDIENTES_ID_GENERATOR", sequenceName = "CLINICAUNA.SEQ_ENCABEZADOEXPEDIENTE", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TB_ENCABEZADOEXPEDIENTES_ID_GENERATOR")
    @Basic(optional = false)
    @Column(name = "IDENCABEXP")
    private Long idencabexp;
    @Column(name = "ALERGIASENCABEXP")
    private String alergiasencabexp;
    @Column(name = "PATOLOGICOENCABEXP")
    private String patologicoencabexp;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idencabexp", fetch = FetchType.LAZY)
    private List<TbDetalleexpediente> tbDetalleexpedienteList;
    @JoinColumn(name = "IDPACIENTE", referencedColumnName = "IDPACIENTE")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private TbRegistropacientes idpaciente;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idencabexp", fetch = FetchType.LAZY)
    private List<TbExamenespaciente> tbExamenespacienteList;

    public TbEncabezadoexpediente() {
    }

    public TbEncabezadoexpediente(EncabezadoexpedienteDto encaExp) {
        this.hospitalizacionesencabexp = encaExp.getHospitalizacionesencabexp();
        if(encaExp.getIdencabexp()!=null){
        this.idencabexp = encaExp.getIdencabexp();
        }
        this.operacionesencabexp = encaExp.getOperacionesencabexp();
        this.alergiasencabexp = encaExp.getAlergiasencabexp();
        this.patologicoencabexp = encaExp.getPatologicoencabexp();
        this.idpaciente = new TbRegistropacientes(encaExp.getIdpaciente());
    }

    public TbEncabezadoexpediente(Integer hospitalizacionesencabexp, Integer versionencabexp, Long idencabexp, 
        Integer operacionesencabexp, String alergiasencabexp, String tratamientosencabexp, String enfermheredencabexp,
        String parentescoencabexp, String patologicoencabexp, List<TbDetalleexpediente> tbDetalleexpedienteList, TbRegistropacientes idpaciente, List<TbExamenespaciente> tbExamenespacienteList) {
        this.hospitalizacionesencabexp = hospitalizacionesencabexp;
        this.versionencabexp = versionencabexp;
        this.idencabexp = idencabexp;
        this.operacionesencabexp = operacionesencabexp;
        this.alergiasencabexp = alergiasencabexp;
        this.patologicoencabexp = patologicoencabexp;
        this.tbDetalleexpedienteList = tbDetalleexpedienteList;
        this.idpaciente = idpaciente;
        this.tbExamenespacienteList = tbExamenespacienteList;
    }
    
    public void actualizarEncabExp(EncabezadoexpedienteDto encabexpDto){
        this.idencabexp = encabexpDto.getIdencabexp();
        this.hospitalizacionesencabexp = encabexpDto.getHospitalizacionesencabexp();
        this.operacionesencabexp = encabexpDto.getOperacionesencabexp();
        this.alergiasencabexp = encabexpDto.getAlergiasencabexp();
        this.patologicoencabexp = encabexpDto.getPatologicoencabexp();
        this.idpaciente = new TbRegistropacientes(encabexpDto.getIdpaciente());
    }

    public TbEncabezadoexpediente(Long idencabexp) {
        this.idencabexp = idencabexp;
    }

    public Long getIdencabexp() {
        return idencabexp;
    }

    public void setIdencabexp(Long idencabexp) {
        this.idencabexp = idencabexp;
    }



    public String getAlergiasencabexp() {
        return alergiasencabexp;
    }

    public void setAlergiasencabexp(String alergiasencabexp) {
        this.alergiasencabexp = alergiasencabexp;
    }

    public String getPatologicoencabexp() {
        return patologicoencabexp;
    }

    public void setPatologicoencabexp(String patologicoencabexp) {
        this.patologicoencabexp = patologicoencabexp;
    }

    @XmlTransient
    public List<TbDetalleexpediente> getTbDetalleexpedienteList() {
        return tbDetalleexpedienteList;
    }

    public void setTbDetalleexpedienteList(List<TbDetalleexpediente> tbDetalleexpedienteList) {
        this.tbDetalleexpedienteList = tbDetalleexpedienteList;
    }

    public TbRegistropacientes getIdpaciente() {
        return idpaciente;
    }

    public void setIdpaciente(TbRegistropacientes idpaciente) {
        this.idpaciente = idpaciente;
    }

    @XmlTransient
    public List<TbExamenespaciente> getTbExamenespacienteList() {
        return tbExamenespacienteList;
    }

    public void setTbExamenespacienteList(List<TbExamenespaciente> tbExamenespacienteList) {
        this.tbExamenespacienteList = tbExamenespacienteList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idencabexp != null ? idencabexp.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbEncabezadoexpediente)) {
            return false;
        }
        TbEncabezadoexpediente other = (TbEncabezadoexpediente) object;
        if ((this.idencabexp == null && other.idencabexp != null) || (this.idencabexp != null && !this.idencabexp.equals(other.idencabexp))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "clinicaunaWS.model.TbEncabezadoexpediente[ idencabexp=" + idencabexp + " ]";
    }


    @XmlTransient
    public List<TbAntecedentesheredofam> getTbAntecedentesheredofamList() {
        return tbAntecedentesheredofamList;
    }

    public void setTbAntecedentesheredofamList(List<TbAntecedentesheredofam> tbAntecedentesheredofamList) {
        this.tbAntecedentesheredofamList = tbAntecedentesheredofamList;
    }

    public Integer getVersionencabexp() {
        return versionencabexp;
    }

    public void setVersionencabexp(Integer versionencabexp) {
        this.versionencabexp = versionencabexp;
    }

    public Integer getHospitalizacionesencabexp() {
        return hospitalizacionesencabexp;
    }

    public void setHospitalizacionesencabexp(Integer hospitalizacionesencabexp) {
        this.hospitalizacionesencabexp = hospitalizacionesencabexp;
    }

    public Integer getOperacionesencabexp() {
        return operacionesencabexp;
    }

    public void setOperacionesencabexp(Integer operacionesencabexp) {
        this.operacionesencabexp = operacionesencabexp;
    }
    
}
