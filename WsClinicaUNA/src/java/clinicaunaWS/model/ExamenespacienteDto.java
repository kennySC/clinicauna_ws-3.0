// DTO DEL WEBSERVICE //

package clinicaunaWS.model;

import clinicaunaWS.util.LocalDateAdapter;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

/**
 *
 * @author Matthew Miranda
 */

@XmlRootElement(name = "ExamenespacienteDto")
@XmlAccessorType(XmlAccessType.FIELD)
public class ExamenespacienteDto {
    
    private Long idexamp;
    private String nombreexamp;
    @XmlJavaTypeAdapter(LocalDateAdapter.class)
    private LocalDate fechaexamp;
    private String anotacionesexamp;
    private String archivoexamp;
    private EncabezadoexpedienteDto idencabexp; // FK
   
    // CONSTRUCTOR VACIO //
    public ExamenespacienteDto(){
        
    }

    public ExamenespacienteDto(Long idexamp, String nombreexamp, LocalDate fechaexamp, String anotacionesexamp, RegistropacientesDto idpaciente, DetalleexpedienteDto iddetexp, EncabezadoexpedienteDto idencexp) {
        this.idexamp = idexamp;
        this.nombreexamp = nombreexamp;
        this.fechaexamp = fechaexamp;
        this.anotacionesexamp = anotacionesexamp;
        this.idencabexp = idencexp;
    }
    
    public ExamenespacienteDto(TbExamenespaciente exampaciente){
        this.idexamp = exampaciente.getIdexamp();
        this.nombreexamp = exampaciente.getNombreexamp();
        this.fechaexamp = exampaciente.getFechaexamp().toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDate();
        this.anotacionesexamp = exampaciente.getAnotacionesexamp();
        this.archivoexamp = exampaciente.getArchivoexamp().toString();
        this.idencabexp = new EncabezadoexpedienteDto(exampaciente.getIdencabexp());
    }

    public Long getIdexamp() {
        return idexamp;
    }

    public void setIdexamp(Long idexamp) {
        this.idexamp = idexamp;
    }

    public String getNombreexamp() {
        return nombreexamp;
    }

    public void setNombreexamp(String nombreexamp) {
        this.nombreexamp = nombreexamp;
    }

    public LocalDate getFechaexamp() {
        return fechaexamp;
    }

    public void setFechaexamp(LocalDate fechaexamp) {
        this.fechaexamp = fechaexamp;
    }

    public String getAnotacionesexamp() {
        return anotacionesexamp;
    }

    public void setAnotacionesexamp(String anotacionesexamp) {
        this.anotacionesexamp = anotacionesexamp;
    }

    public EncabezadoexpedienteDto getIdencabexp() {
        return idencabexp;
    }

    public void setIdencabexp(EncabezadoexpedienteDto idencexp) {
        this.idencabexp = idencexp;
    }

    @Override
    public String toString() {
        return "ExamenespacienteDto{" + "idexamp=" + idexamp + ", nombreexamp=" + nombreexamp + 
                ", fechaexamp=" + fechaexamp + ", anotacionesexamp=" + anotacionesexamp + ", iddetexp=" + idencabexp + '}';
    }

    public String getArchivoexamp() {
        return archivoexamp;
    }

    public void setArchivoexamp(String archivoexamp) {
        this.archivoexamp = archivoexamp;
    }
    
    
    
}
