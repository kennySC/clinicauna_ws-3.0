// DTO DEL WEBSERVICE //

package clinicaunaWS.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Matthew Miranda
 */
@XmlRootElement(name = "AntecedentesheredofamDto")
@XmlAccessorType(XmlAccessType.FIELD)
public class AntecedentesheredofamDto {
    
    private Long idantecedheredfam;
    private String enfermheredanteced;
    private String parentescoanteced;
    // FK //
    private EncabezadoexpedienteDto idencabexp;
    
    private boolean nuevo;
    
    public AntecedentesheredofamDto(){
    }
    
    public AntecedentesheredofamDto(TbAntecedentesheredofam antecedHF){
        this.idantecedheredfam = antecedHF.getIdantecedheredfam();
        this.enfermheredanteced = antecedHF.getEnfermheredanteced();
        this.parentescoanteced = antecedHF.getParentescoanteced();
    }

    public Long getIdantecedheredfam() {
        return idantecedheredfam;
    }

    public void setIdantecedheredfam(Long idantecedheredfam) {
        this.idantecedheredfam = idantecedheredfam;
    }

    public String getEnfermheredanteced() {
        return enfermheredanteced;
    }

    public void setEnfermheredanteced(String enfermheredanteced) {
        this.enfermheredanteced = enfermheredanteced;
    }

    public String getParentescoanteced() {
        return parentescoanteced;
    }

    public void setParentescoanteced(String parentescoanteced) {
        this.parentescoanteced = parentescoanteced;
    }

    public EncabezadoexpedienteDto getIdencabexp() {
        return idencabexp;
    }

    public void setIdencabexp(EncabezadoexpedienteDto idencabexp) {
        this.idencabexp = idencabexp;
    }
    
    public boolean isNuevo() {
        return nuevo;
    }

    public void setNuevo(boolean nuevo) {
        this.nuevo = nuevo;
    }

    @Override
    public String toString() {
        return "AntecedentesheredofamDto{" + "idantecedheredfam=" + idantecedheredfam + 
                ", enfermheredanteced=" + enfermheredanteced + ", parentescoanteced=" + parentescoanteced + 
                ", idencabexp=" + idencabexp + '}';
    }
    
    
    
}
