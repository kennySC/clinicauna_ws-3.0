/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinicaunaWS.services;

import clinicaunaWS.util.CodigoRespuesta;
import clinicaunaWS.util.Respuesta;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;

/**
 *
 * @author Kenneth Sibaja
 */
@Stateless
@LocalBean
public class ReportesService {

    @PersistenceContext(unitName = "WsClinicaUNAPU")
    private EntityManager em;

    public Respuesta getReporteExpediente(Long idPaciente) {
        try {

            Connection conn = em.unwrap(Connection.class);
            
            JasperReport rep = JasperCompileManager.compileReport(getClass().getResourceAsStream("../Reportes/reporteExpediente.jrxml"));
            Map parametros = new HashMap();
            parametros.put("idpaciente", idPaciente);

            JasperPrint report = JasperFillManager.fillReport(rep, parametros, conn);

//            System.out.println("llego al reporte");
//            System.out.println(" Pages size: " + report.getPages().size());
//            System.out.println(report.getName());
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "reporte", report);

        } catch (Exception ex) {
            Logger.getLogger(ReportesService.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
            return new Respuesta(false, CodigoRespuesta.ERROR_ACCESO, "Error al guardar a la persona", "guardarPersona" + ex.getMessage());
        }
    }

    public Respuesta getReporteAgenda(Long idMedico, Date f1, Date f2) {
        try {

            Connection conn = em.unwrap(Connection.class);

            JasperReport rep = JasperCompileManager.compileReport(getClass().getResourceAsStream("../Reportes/reporteAgendaRangoDias.jrxml"));
            Map parametros = new HashMap();
            parametros.put("idMedico", idMedico);
            parametros.put("fecha1", f1);
            parametros.put("fecha2", f2);

            JasperPrint report = JasperFillManager.fillReport(rep, parametros, conn);

            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "reporte", report);

        } catch (Exception ex) {

            Logger.getLogger(ReportesService.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
            return new Respuesta(false, CodigoRespuesta.ERROR_ACCESO, "Error al guardar a la persona", "guardarPersona" + ex.getMessage());

        }
    }

}
