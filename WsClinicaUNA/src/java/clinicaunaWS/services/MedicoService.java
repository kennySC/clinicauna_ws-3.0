package clinicaunaWS.services;

import clinicaunaWS.model.MantmedicosDto;
import clinicaunaWS.model.TbMantmedicos;
import clinicaunaWS.util.CodigoRespuesta;
import clinicaunaWS.util.Respuesta;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author DJork
 */
@Stateless
@LocalBean
public class MedicoService {

    private static final Logger LOG = Logger.getLogger(MedicoService.class.getName());

    @PersistenceContext(unitName = "WsClinicaUNAPU")
    private EntityManager em;

    public Respuesta guardarMedico(MantmedicosDto medDto) {

        try {
            TbMantmedicos medico;
            // PRIMERO CONSULTAMOS SI EL PACIENTE YA EXISTE, DE SER ASI, SUS DATOS SE ACTUALIZAN //
            if (medDto.getIdmed() != null && medDto.getIdmed() > 0) {
                medico = em.find(TbMantmedicos.class, medDto.getIdmed());
                if (medico == null) {
                    return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Error al guardar el medico", "guardarMedico NoResultExcepton");
                }
                medico.actualizarMedico(medDto);
                medico = em.merge(medico);
            } // SI NO EXISTE EL PACIENTE ENTONCES SE PROCEDE A CREAR UNO NUEVO //
            else {
                medico = new TbMantmedicos(medDto);
                em.persist(medico);
            }
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Medico", new MantmedicosDto(medico));
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error guardando el medico.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Error al guardar el medico", "guardarMedico" + ex.getMessage());
        }
    }

    public Respuesta getMedicos(String nombre, String apellidos, String cedula/*, Integer carne*/) {
        try {
            Query qryMedicos = em.createNamedQuery("TbMantmedicos.findByIdPers_nom_ape_ced_carne", TbMantmedicos.class);
            qryMedicos.setParameter("cedula", cedula);
            qryMedicos.setParameter("nombre", nombre);
            qryMedicos.setParameter("apellidos", apellidos);
            //qryMedicos.setParameter("carne", carne);
            List<TbMantmedicos> listmeds = qryMedicos.getResultList();
            List<MantmedicosDto> listmedsDto = new ArrayList<>();
            for (TbMantmedicos medico : listmeds) {
                listmedsDto.add(new MantmedicosDto(medico));
            }
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Medicos", listmedsDto);
        } catch (Exception e) {
            LOG.log(Level.SEVERE, "Ocurrio un error al obtener al medico.", e);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al obtener el medico.", "getMedicos " + e.getMessage());
        }

    }

    public Respuesta eliminarMedico(Long id) {
        try {
            TbMantmedicos elimMed;
            if (id != null && id > 0) {
                elimMed = em.find(TbMantmedicos.class, id);
                if (elimMed == null) {
                    return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No se encrontró el medico a eliminar.", "eliminarMedico NoResultException");
                }
                em.remove(elimMed);
            } else {
                return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "Debe cargar el medico a eliminar.", "eliminarMedico NoResultException");
            }
            em.flush();
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "");
        } catch (Exception e) {
            if (e.getCause() != null && e.getCause().getCause().getClass() == SQLIntegrityConstraintViolationException.class) {
                return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "No se puede eliminar al medico ...", "eliminarMedico " + e.getMessage());
            }
            LOG.log(Level.SEVERE, "Ocurrio un error al guardar el empleado.", e);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al eliminar al medico.", "eliminarMedico" + e.getMessage());
        }
    }

}
