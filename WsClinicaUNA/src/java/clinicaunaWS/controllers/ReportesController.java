/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinicaunaWS.controllers;

import clinicaunaWS.services.ReportesService;
import clinicaunaWS.util.CodigoRespuesta;
import clinicaunaWS.util.Respuesta;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.sql.Date;
import java.util.Base64;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.inject.Inject;
import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import net.sf.jasperreports.engine.JasperPrint;

/**
 *
 * @author Kenneth Sibaja
 */
@Path("/ReportesController")
public class ReportesController {

    @EJB
    ReportesService reportesService = new ReportesService();

    @GET
    @Path("/generarReporteExpediente/{idPaciente}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response generarReporteExpediente(@PathParam("idPaciente") long idPaciente) {
        try {

            Respuesta res = reportesService.getReporteExpediente(idPaciente);
            if (res.getEstado()) {
                
                JasperPrint report = (JasperPrint) res.getResultado("reporte");
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                ObjectOutputStream os = new ObjectOutputStream(bos);
                os.writeObject(report);
                byte[] temporalReporte = bos.toByteArray();
                
                return Response.ok(temporalReporte).build();
            }
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error obteniendo el Reporte").build();
        } catch (Exception ex) {
            Logger.getLogger(ReportesController.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error obteniendo el Reporte").build();
        }
    }

    @GET
    @Path("/generarReporteAgenda/{idMed}/{fecha1}/{fecha2}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response generarReporteAgenda(@PathParam("idMed") Long idMed, @PathParam("fecha1") String fecha1, @PathParam("fecha2") String fecha2) {
        try {

            Date date1 = Date.valueOf(fecha1);
            Date date2 = Date.valueOf(fecha2);

            Respuesta res = reportesService.getReporteAgenda(idMed, date1, date2);
            if (res.getEstado()) {
                
                JasperPrint report = (JasperPrint) res.getResultado("reporte");
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                ObjectOutputStream os = new ObjectOutputStream(bos);
                os.writeObject(report);
                byte[] temporalReporte = bos.toByteArray();

                return Response.ok(temporalReporte).build();
            }
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error obteniendo el Reporte").build();
        } catch (Exception ex) {
            Logger.getLogger(ReportesController.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error obteniendo el Reporte").build();
        }
    }

}
