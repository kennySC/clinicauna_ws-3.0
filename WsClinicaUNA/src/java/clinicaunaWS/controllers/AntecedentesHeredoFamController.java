package clinicaunaWS.controllers;

import clinicaunaWS.model.AntecedentesheredofamDto;
import clinicaunaWS.services.AntecedentesHeredoFamService;
import clinicaunaWS.util.CodigoRespuesta;
import clinicaunaWS.util.Respuesta;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Matthew Miranda
 */
@Path("/AntecedentesHFController")
public class AntecedentesHeredoFamController {

    @EJB
    AntecedentesHeredoFamService antecedhfService;

    @POST
    @Path("/guardarAntecedente")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response guardarAntecedHF(AntecedentesheredofamDto antecedhfDto) {
        try {
            Respuesta res = antecedhfService.guardarAntecedenteHF(antecedhfDto);
            if(!res.getEstado()){
                return Response.status(res.getCodigoRespuesta().getValue()).build();
            }
            return Response.ok(res.getCodigoRespuesta().getValue()).build();
        } catch (Exception ex) {
            Logger.getLogger(PacienteController.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error Guardando el detalle del expediente").build();
        }
    }

    @DELETE
    @Path("/eliminarAntecedente/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response eliminarAntecedente(@PathParam("id") Long id) {
        try {
            Respuesta resp = antecedhfService.eliminarAntecedente(id);
            if (!resp.getEstado()) {
                return Response.status(resp.getCodigoRespuesta().getValue()).entity(resp.getMensaje()).build();
            }
            return Response.ok().build();
        } catch (Exception e) {
            Logger.getLogger(PersonaController.class.getName()).log(Level.SEVERE, null, e);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error eliminando al Antecedente").build();
        }
    }

    @GET
    @Path("/Antecedentes/{idExpediente}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAntecedentes(@PathParam("idExpediente") Long idExpediente) {
        try {
            Respuesta res = antecedhfService.getAntecedentes(idExpediente);
            if (!res.getEstado()) {
                return Response.status(res.getCodigoRespuesta().getValue()).entity(res.getMensaje()).build();
            }
            return Response.ok(new GenericEntity<List<AntecedentesheredofamDto>>((List<AntecedentesheredofamDto>) res.getResultado("Antecedentes")) {
            }).build();
        } catch (Exception ex) {
            Logger.getLogger(AntecedentesHeredoFamController.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error obteniendo los Antecedentes").build();
        }
    }
    
}
